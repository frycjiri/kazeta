﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace KazetaNode.Coms
{
    enum Channels
    {
        Discovery,Capture,Deliver,Process, Persist
    }

    class Postman
{
        public delegate bool ThenAction(List<IMessage> messages);
        public delegate void DuringAction(IMessage message);
        public delegate bool TimeoutAction();
        public long TimeoutMax { get; private set; }
        public long TimeStart { get; set; } = -1;
        public TimeoutAction TimeoutAc { get; private set; }
        public ThenAction ThenAc { get; private set; } = null;
        public DuringAction DuringAc { get; private set; } = null;
        public bool Multiple { get; private set; } = false;

        public string Exchange { get; private set; }
        public string Route { get; private set; }
        public IMessage Message { get; private set; }

        protected internal List<IMessage> Responses = new List<IMessage>();

        public static Postman SendToNode(string node,IMessage message)
        {
            return new Postman
            {
                Exchange = "",
                Route = "app.kazeta.node."+node,
                Message = message
            };
        }
        public static Postman SendToChannel(Channels channel,IMessage message)
        {
            string node = "";
            switch(channel)
            {
                case Channels.Discovery: node = Network.ROUTING_DISCOVERY; break;
                case Channels.Capture: node = "kazeta.capture"; break;
                case Channels.Deliver: node = "kazeta.deliver"; break;
                case Channels.Persist: node = "kazeta.persist"; break;
                case Channels.Process: node = "kazeta.process"; break;
            }
            return new Postman
            {
                Exchange = Network.EXCHANGE,
                Route = node,
                Message = message
            };
        }
        public Postman Timeout(long miliseconds,TimeoutAction action=null)
        {
            if (TimeStart != -1)
                throw new Exception("Already started");
            this.TimeoutAc = action;
            this.TimeoutMax = miliseconds;
            return this;
        }
        public Postman Then(ThenAction action)
        {
            if (TimeStart != -1)
                throw new Exception("Already started");
            this.ThenAc = action;
            return this;
        }
        public Postman During(DuringAction action)
        {
            if (TimeStart != -1)
                throw new Exception("Already started");
            this.DuringAc = action;
            return this;
        }
        public Postman ExpectSingle()
        {
            if (TimeStart != -1)
                throw new Exception("Already started");
            this.Multiple = false;
            return this;
        }
        public Postman ExpectMultiple()
        {
            if (TimeStart != -1)
                throw new Exception("Already started");
            this.Multiple = true;
            return this;
        }
        public void Send(bool expectresponse = true)
        {
            if (TimeStart != -1)
                throw new Exception("Already started");
            Program.Network.SendPostMan(this, expectresponse);
        }
    }
}
