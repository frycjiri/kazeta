﻿using KazetaNode.Modules.Capture.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace KazetaNode.Coms
{
    abstract class IMessage
    {
        [JsonProperty("message_id")]
        public string MessageId { get; set; } = Guid.NewGuid().ToString();
        [JsonProperty("response_id")]
        public string ResponseId { get; set; }
        [JsonProperty("sender")]
        public string Sender { get; set; }
        [JsonProperty("message_type")]
        public string MessageType { get { return this.GetType().FullName; } }

        public Postman Response(IMessage msg)
        {
            msg.ResponseId = this.MessageId;
            return Postman.SendToNode(this.Sender, msg);
        }

    }
    class TestMessage : IMessage
    {
    }
    class DiscoveryRequest : IMessage
    {
    }
    class DiscoveryResponse : IMessage
    {
    }

    class DiscoveryMessage : IMessage
    {
    }
    class DiscoveryReplyMessage : IMessage
    {
        public bool Capture { get; set; }
        public bool Persist { get; set; }
        public bool Process { get; set; }
        public bool Deliver { get; set; }
    }
}
