﻿using KazetaNode.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace KazetaNode.Coms
{

    class Network
    {
        public delegate void ReceiveMessage(IMessage message);
        string id;
        IConnection con;
        public IModel channel;
        ConnectionFactory factory;
        List<Postman> Postmans = new List<Postman>();
        Dictionary<Type, List<ReceiveMessage>> Handlers=new Dictionary<Type, List<Network.ReceiveMessage>>();
        Thread TimeoutThread;
        Stopwatch watch;
        int timeout;
        public Network(dynamic config)
        {
            id = config["id"];
            QUEUE = QUEUE_PREFIX + id;
            factory = new ConnectionFactory
            {
                UserName = config["network"]["user"],
                Password = config["network"]["password"],
                VirtualHost = config["network"]["virtual_host"],
                HostName = config["network"]["hostname"],
                Port = Int32.Parse(config["network"]["port"])
            };
            timeout = Int32.Parse(config["experimentional"]["net_timeout"]);
            watch = new Stopwatch();

            TimeoutThread = new Thread(new ThreadStart(delegate
            {
                while(true)
                { 
                Thread.Sleep(timeout);
                    for (int i = Postmans.Count - 1; i >= 0; i--)
                {
                        Postman p = Postmans[i];
                    if (watch.ElapsedMilliseconds>p.TimeoutMax+p.TimeStart)
                    {
                        if (p.Responses.Count > 0)
                            p.ThenAc?.Invoke(p.Responses);
                        else p.TimeoutAc?.Invoke();
                            Postmans.Remove(p);
                    }
                }
                }
            }));
            
        }

        public static readonly string EXCHANGE = "kazeta.network";
        public static readonly string ROUTING_DISCOVERY = "kazeta.route.discovery";
        public static readonly string QUEUE_PREFIX = "app.kazeta.node.";
        private readonly string QUEUE;


        public static void DiscoveryReply(IMessage msg)
        {
            Program.Network.channel.BasicPublish("", QUEUE_PREFIX+msg.Sender, null, Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(new DiscoveryReplyMessage() { ResponseId = msg.MessageId,Sender=Program.NodeId, Capture=Program.Capture!=null,Process=Program.Process!=null,Persist=Program.Persist!=null, Deliver=Program.Deliver!=null })));

        }
        public void Connect()
        {
            TimeoutThread.Start();
            watch.Start();
            con = factory.CreateConnection();
            channel = con.CreateModel();
            channel.ExchangeDeclare(EXCHANGE, ExchangeType.Topic, true, false);
            channel.QueueDeclare(QUEUE, true,true,false);
            channel.QueueBind(queue: QUEUE, exchange: EXCHANGE, routingKey: ROUTING_DISCOVERY);
            RegisterHandler(typeof(DiscoveryMessage), DiscoveryReply);
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
             {
                 var json = Encoding.UTF8.GetString(ea.Body);
                 JObject dyn = JsonConvert.DeserializeObject<JObject>(json);
                 var st = dyn.GetValue("message_type").ToString();
                 Type type = typeof(IMessage).Assembly.GetType(st);
                 IMessage msg = (IMessage)JsonConvert.DeserializeObject(json,type);
                 for (int i = Postmans.Count - 1; i >= 0; i--)
                 {
                     Postman p = Postmans[i];
                     if (p.Message.MessageId.Equals(msg.ResponseId))
                     {
                         p.DuringAc?.Invoke(msg);
                         p.Responses.Add(msg);
                         if (!p.Multiple)
                         {
                             Postmans.Remove(p);
                             p.ThenAc?.Invoke(p.Responses);
                         }
                         return;
                     }
                 }
                 if(Handlers.ContainsKey(msg.GetType()))
                 {
                     foreach (ReceiveMessage recv in Handlers[msg.GetType()])
                         recv.Invoke(msg);
                 }
             };

            channel.BasicConsume(queue: "app.kazeta.node." + id,
                                 autoAck: true,
                                 consumer: consumer);
        }

        internal void SendPostMan(Postman postman, bool expectresponse=true)
        {
            postman.TimeStart = watch.ElapsedMilliseconds;
            if(expectresponse)
                Postmans.Add(postman);
            postman.Message.Sender = Program.NodeId;
            Program.Network.channel.BasicPublish(postman.Exchange, postman.Route, null, Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(postman.Message)));
        }

        public void RegisterHandler(Type message, ReceiveMessage handler)
        {
            if (!Handlers.ContainsKey(message))
                Handlers.Add(message, new List<ReceiveMessage>());
            Handlers[message].Add(handler);
        }

        public void TestConnection()
        {
            var test = new TestMessage();
            test.ResponseId = test.MessageId;
            Postman.SendToNode("app.kazeta.node." + id, test).ExpectSingle().Timeout(9000).Then(a => {
                ConsoleP.AppendLine("Network: Test complete (send: ok, recv: ok)");
                return true;
            }).Send();
        }
        public void Close()
        {
            channel.Close();
            con.Close();
        }
    }
}
