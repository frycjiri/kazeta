﻿using KazetaNode.Coms;
using KazetaNode.Libraries;
using System;
using System.Collections.Generic;
using System.Text;

namespace KazetaNode
{
    abstract class Command
    {
        public Command()
        {

        }
        public abstract string[] Aliases { get; }
        public abstract void Execute(AbstractLibraryInstance instance, CommandInput input);

    }
    abstract class Command<T> : Command where T:AbstractLibrary
    {
        protected internal T library;
        public Command(T library)
        {
            this.library = library;
        }

    }
    class CmdTestNetwork : Command
    {
        public override string[] Aliases => new string[] { "test-network" };

        public override void Execute(AbstractLibraryInstance instance, CommandInput input)
        {
            Program.Network.TestConnection();
        }
    }
    class CmdModules : Command
    {
        public override string[] Aliases => new string[] { "modules" };

        public override void Execute(AbstractLibraryInstance instance, CommandInput input)
        {
            ConsoleP.AppendLine("Modules:");
            ConsoleP.AppendLine(" - Capture", Program.Capture != null ? "@green" : "@red", Program.Capture != null ? "Enabled" : "Disabled");
            ConsoleP.AppendLine(" - Persist", Program.Persist != null ? "@green" : "@red", Program.Persist != null ? "Enabled" : "Disabled");
            ConsoleP.AppendLine(" - Process", Program.Process != null ? "@green" : "@red", Program.Process != null ? "Enabled" : "Disabled");
            ConsoleP.AppendLine(" - Deliver", Program.Deliver != null ? "@green" : "@red", Program.Deliver != null ? "Enabled" : "Disabled");
        }
    }
    class CmdClear : Command
    {
        public override string[] Aliases => new string[] { "clear" };

        public override void Execute(AbstractLibraryInstance instance, CommandInput input)
        {
            ConsoleP.Clear();
        }
    }
    class CmdLibraries : Command
    {
        public override string[] Aliases => new string[] { "libraries" };

        public override void Execute(AbstractLibraryInstance instance, CommandInput input)
        {
            ConsoleP.AppendDelimeter();
            ConsoleP.AppendCols(new int[] { 20, 30, 10, 10 }, "Id", "Name","Version","State");
            ConsoleP.AppendDelimeter();
            foreach (var lib in Program.Libraries)
            {
                ConsoleP.AppendCols(new int[] { 20,30,10,10 },lib.Id, lib.Name, lib.Version, "@green", "Enabled");
            }
            ConsoleP.AppendDelimeter();
        }
    }
    class CmdNetworkDiscovery : Command
    {
        public override string[] Aliases => new string[] { "network-discovery" };

        public override void Execute(AbstractLibraryInstance instance, CommandInput input)
        {
            ConsoleP.AppendLine("Starting network discovery protocol... (Timeout 5s)");
            Postman.SendToChannel(Channels.Discovery, new DiscoveryMessage()).ExpectMultiple().Timeout(5_000)
                .During(msg=> {
                    DiscoveryReplyMessage m = (DiscoveryReplyMessage) msg;
                    ConsoleP.AppendLine("Discovered node '" + msg.Sender + "':","@green",m.Capture?"Capture ":"",m.Process?"Process ":"",m.Persist?"Persist ":"",m.Deliver?"Deliver":"");
                })
                .Then(msgs=>{
                    ConsoleP.AppendLine("Network discovery protocol ended.");
                return true;
            }).Send();
        }
    }
    class CommandInput
    {
        public string Id { get; private set; }
        public Dictionary<string, string> Parameters { get; private set; } = new Dictionary<string, string>();
        public List<string> Arguments { get; private set; } = new List<string>();

        private CommandInput()
        {
        }
        /**
         * dmx -q=10
         */
        public static CommandInput ParseLine(string line)
        {
            var input = new CommandInput()
            {
                Id = ""
            };
            line = line.Trim();
            int i = 0;
            int max = line.Length;
            while (i < max && !line[i].Equals(' '))
            {
                input.Id += line[i];
                i++;
            }
            if (input.Id.Length == 0)
                throw new Exception();
            
            /**
             * 0 - searching for next
             * 1 - parameter name read
             * 2 - parameter value read
             * 3 - parameter string value read
             * 4 - argument read
             * 5 - argument string read
             */
            int mode = 0;
            string name="", value="";
            i++;
            while (i < max)
            {
                if (line[i].Equals(' '))
                {
                    if (mode == 0) { i++; continue; }
                    if (mode == 1) { i++; input.Parameters.Add(name, null); name = ""; mode = 0; continue; }
                    if (mode == 2) { i++; input.Parameters.Add(name, value); name = ""; value = ""; mode = 0; continue; }
                    if (mode == 4) { i++; input.Arguments.Add(value); value = ""; mode = 0; continue; }
                }
                else if (mode==0 && line[i].Equals('-'))
                {
                    i++;
                    mode = 1; continue;
                }
                else if (mode == 1 && line[i].Equals('='))
                {
                    i++;
                    mode = 2; continue;
                }
                else if ((mode == 2 || mode == 4) && line[i].Equals('"'))
                {
                    i++;
                    mode++; continue;
                }
                else if (line[i].Equals('"') && value.Length>0 && !value[value.Length-1].Equals('\\'))
                {
                    if (mode == 3)
                    {
                        i++; input.Parameters.Add(name, value); name = ""; value = ""; mode = 0; continue; }
                    if (mode == 5)
                    {
                        i++; input.Arguments.Add(value); value = ""; mode = 0; continue; }
                }

                if(mode==0) { mode = 4; value += line[i]; }
                else if(mode==1) { name += line[i]; }
                else { value += line[i]; }
                i++;
            }
            if (value.Length > 0 || name.Length>0)
                if (mode >= 4)
                    input.Arguments.Add(value);
                else
                    input.Parameters.Add(name, value);
            return input;
        }
    }

}
