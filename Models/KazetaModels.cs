﻿using KazetaNode.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace KazetaNode.Models
{
    class KazetaNodeIdentifier
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
    enum ProjectStage
    {
        NOT_STARTED=1,CAPTURE_WAITING=2,CAPTURE_RUNNING=3,CAPTURE_UPLOADING=4,PERSIST_UPLOADING=5,PROCESS_WAITING=6,PROCESS_RUNNING=7,PROCESS_UPLOADING=8,PERSIST_READY=9
    }
    enum ProjectFileType
    {
        RAW=1,HELP=2,PROCESSED=3
    }
    class ProjectFile
    {
        public string FileName { get; set; }
        public int Size { get; set; }
        public string Checksum { get; set; }
        public string Source { get; set; }
        public ProjectFileType Type { get; set; }

    }
    class ProjectCapture
    {
        public string Room { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public CaptureMode Mode { get; set; } = CaptureMode.SPEAKER_PRESENTATION_BLACKBOARD;
        public VideoQuality Quality { get; set; } = VideoQuality.V1080p;
        public List<string> Before { get; set; } = new List<string>();
        public List<string> After { get; set; } = new List<string>();
    }
    class ProjectPersistComunication
    {
        public string Type { get; set; }
        public int MaxBandwith { get; set; }
    }
    class ProjectPersist
    {
        public List<string> At { get; set; } = new List<string>();
        public List<ProjectPersistComunication> Communications { get; set; } = new List<ProjectPersistComunication>();
        public bool KeepRawFiles { get; set; } = true;
        public bool KeepHelpFiles { get; set; } = false;
        public bool KeepProcessFiles { get; set; } = true;

    }
    class ProjectProcess
    {
        public List<ProjectProcessMappedFiles> MappedFiles = new List<ProjectProcessMappedFiles>();
        public List<string> Script = new List<string>();
    }
    class ProjectProcessMappedFiles
    {
        public string FileName { get; set; }
        public VideoQuality Quality { get; set; }

    }

    class Project
    {
        public string Id { get; set; }
        public string Author { get; set; }
        public List<string> Notify { get; set; } = new List<string>();
        public ProjectStage Stage { get; set; } = ProjectStage.NOT_STARTED;
        public List<ProjectFile> Files { get; set; } = new List<ProjectFile>();
        public ProjectCapture Capture { get; set; } = new ProjectCapture();
        public ProjectProcess Process { get; set; } = new ProjectProcess();
        public ProjectPersist Persist { get; set; } = new ProjectPersist();

    }
}
