﻿using System;
using System.Collections.Generic;
using System.Text;
using YamlDotNet.Serialization;

namespace KazetaNode.Models
{
    class NdiSource
    {
        public string Id { get; set; }
        public string Ip { get; set; }
        public ushort Port { get; set; }
    }
    class NdiSourceDescriptor : NdiSource
    {
        [YamlMember(Alias = "video_capture",ApplyNamingConventions =false)]
        public bool VideoCapture { get; set; }
        [YamlMember(Alias = "audio_capture", ApplyNamingConventions = false)]
        public bool AudioCapture { get; set; }
        public int Priority { get; set; }
        [YamlMember(Alias = "display_name", ApplyNamingConventions = false)]
        public string DisplayName { get; set; }
    }
    class Room
    {
        public string Id { get; set; }
        public List<NdiSourceDescriptor> Sources { get; set; } = new List<NdiSourceDescriptor>();

        public List<string> Initialize { get; set; }
        public List<string> Terminate { get; set; }
    }
    class RoomDefinition : Room
    {

        public CaptureMode[] Modes { get; set; }
        public VideoQuality[] Capability { get; set; }
    }
    
}
