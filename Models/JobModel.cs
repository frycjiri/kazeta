﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KazetaNode.Models
{
    class WorkOrder
    {
        public string Id { get; set; }
        public string Author { get; set; }
        public string NameCs { get; set; }
        public string NameEn { get; set; }
        public List<string> Speakers { get; set; }
        public string Fingerprint { get; set; }
        public CaptureJob Capture { get; set; }
        public ProcessJob Process { get; set; }
        public PersistJob Persist { get; set; }
        public DeliverJob Deliver { get; set; }
    }
    enum JobState { NOT_STARTED, RUNNING, FAILED, COMPLETE }
    enum VideoQuality { V480p,V720p,V1080p,V2k,V4k }
    enum CaptureMode { SPEAKER_PRESENTATION_BLACKBOARD,SPEAKER_PRESENTATION,SPEAKER_BLACKBOARD,SPEAKER,BLACKBOARD,AUDIO,ALL}
    abstract class AbstractJob
    {
        public JobState State { get; set; } = JobState.NOT_STARTED;
        public List<string> Metadata { get; set; } = new List<string>();
    }
    class CaptureJob : AbstractJob
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public Room Room { get; set; }
        public CaptureMode Mode { get; set; }
    }
    class ProcessJob : AbstractJob
    {
        public List<ProcessStep> Steps { get; set; } = new List<ProcessStep>();
    }
    class PersistJob : AbstractJob
    {

    }
    class DeliverJob : AbstractJob
    {

    }


    abstract class ProcessStep
    {

    }
    class StepAddWatermark : ProcessStep
    {

    }
    class StepAddIntro : ProcessStep
    {

    }
    class StepAddOutro : ProcessStep
    {

    }
    class StepConvert : ProcessStep
    {

    }
    class StepMerge : ProcessStep
    {

    }
    class StepCut : ProcessStep
    {

    }
}
