﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace KazetaNode.Helpers
{
    public static class Extensions
    {
        public static void OutputToConsole(this string str)
        {
            Console.SetCursorPosition(0, Console.CursorTop - 1);
            Console.Write(new string(' ', Console.BufferWidth));
            Console.SetCursorPosition(0, Console.CursorTop - 1);
            Console.WriteLine(str);
            Console.Write("kazeta@" + Program.NodeId + ": ");
        }
        public static DateTime GetNextWeekday(this DateTime date,DayOfWeek day)
        {
            DateTime result = date.AddDays(1);
            while (result.DayOfWeek != day)
                result = result.AddDays(1);
            return result;
        }
        public static string HashSha256(this string str)
        {
            var crypt = new SHA256Managed();
            string hash = String.Empty;
            byte[] crypto = crypt.ComputeHash(Encoding.ASCII.GetBytes(str));
            foreach (byte theByte in crypto)
            {
                hash += theByte.ToString("x2");
            }
            return hash;
        }

        public static int ToInt32(this string str)
        {
            return Int32.Parse(str);
        }
        public static string GenerateRandomString(this string str,int size)
        {
            var stringChars = new char[size];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = str[random.Next(str.Length)];
            }
            return new string(stringChars);
        }
        public static string Param(this string str, string name, string value)
        {
            return str.Replace("%" + name + "%", value);
        }
        public static string Param(this string str, string name, int value)
        {
            return str.Replace("%" + name + "%", ""+value);
        }
        public static string ParamAdd(this string str, string value)
        {
            return str + " " + value;
        }
        public static string ParamAdd(this string str, int value)
        {
            return str + " " + value;
        }
    }
}
