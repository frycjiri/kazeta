﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace KazetaNode.Helpers
{
    class FileSystemHelper
    {
        public static long DirSize(DirectoryInfo d)
        {
            long size = 0;
            FileInfo[] fis = d.GetFiles();
            foreach (FileInfo fi in fis)
            {
                size += fi.Length;
            }
            DirectoryInfo[] dis = d.GetDirectories();
            foreach (DirectoryInfo di in dis)
            {
                size += DirSize(di);
            }
            return size;
        }
        public static string GetProjectFolder(string location,ProjectIdDescriptor id)
        {
            return Path.Combine(location, id.Room, id.Year, id.Month,id.Day+"_"+id.Hour+"_"+id.Random);
        }
        public static void CreateProjectFolder(string location,ProjectIdDescriptor id)
        {
            var path = GetProjectFolder(location, id);
            Directory.CreateDirectory(path);
        }
    }
}
