﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KazetaNode.Helpers
{
    class InteractiveWorkBuilder
    {
        public void Start()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Clear();
            Console.WriteLine("========================================");
            Console.WriteLine("            Work builder                ");
            Console.WriteLine("========================================");
            Console.WriteLine("Event name [cs]: ");
            Console.WriteLine("Event name [en]: ");
            Console.WriteLine("Event tags: ");
            Console.WriteLine("Author: ");
            Console.WriteLine("Speakers usernames or fullnames delimetered with ',': ");
            Console.WriteLine("Live stream (y/n): ");
            Console.WriteLine("Capture configuration");
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("Select room: ");
            Console.WriteLine("Capture start date: ");
            Console.WriteLine("Capture end date: ");
            Console.WriteLine("Select capture mode: ");
            Console.WriteLine("1) Speaker with presentation and blackboard (default)");
            Console.WriteLine("2) Speaker with blackboard");
            Console.WriteLine("3) Speaker with presentation");
            Console.WriteLine("4) Speaker");
            Console.WriteLine("5) Blackboard");
            Console.WriteLine("6) Audio only");
            Console.WriteLine("7) Speaker+audiance with presentation and blackboard (GDPR WARNING)");
            Console.WriteLine("Video quality (4k,fullhd,hd,480p): ");

            Console.WriteLine("Process configuration");
            Console.WriteLine("----------------------------------------");

            Console.WriteLine("Delivery configuration");
            Console.WriteLine("----------------------------------------");


        }
    }
}
