﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KazetaNode.Helpers
{
    class ProjectIdGenerator
    {
        public static string Generate(string room,int year,int month,int day,int hour_start,string random)
        {
            return String.Join("_", room, year, month, day, hour_start, random);
        }
    }
    class ProjectIdDescriptor
    {
        private string Id;
        public ProjectIdDescriptor(string id)
        {
            this.Id = id;
        }
        public string Room { get => Id.Split("_")[0]; }
        public string Year { get => Id.Split("_")[1]; }
        public string Month { get => Id.Split("_")[2]; }
        public string Day { get => Id.Split("_")[3]; }
        public string Hour { get => Id.Split("_")[4]; }
        public string Random { get => Id.Split("_")[5]; }
    }
}
