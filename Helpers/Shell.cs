﻿using System.Diagnostics;
using System.IO;

namespace KazetaNode.Helpers
{
    class Shell
    {
        public static ShellResult Execute(string filename, string arguments, int? miliseconds = null)
        {
            Process process = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = filename,
                    Arguments = arguments,
                    RedirectStandardInput = true,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true
                }
            };
            process.Start();
            if (miliseconds == null)
            {
                process.WaitForExit();
            }
            else
            {
                process.WaitForExit((int)miliseconds);
            }

            return new ShellResult()
            {
                Output = process.StandardOutput,
                Error = process.StandardError,
                ExitCode = process.ExitCode
            };

        }
    }
    class ShellResult
    {
        public int ExitCode { get; internal set; }
        public StreamReader Output { get; internal set; }
        public StreamReader Error { get; internal set; }
    }
}
