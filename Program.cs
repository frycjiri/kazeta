﻿using KazetaNode.Coms;
using KazetaNode.Helpers;
using KazetaNode.Libraries;
using KazetaNode.Models;
using KazetaNode.Modules.Capture;
using KazetaNode.Modules.Deliver;
using KazetaNode.Modules.Persist;
using KazetaNode.Modules.Process;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using YamlDotNet.Serialization;

namespace KazetaNode
{
    class Program
    {
        public static readonly string version = "0.2";
        public static string NodeId= "saddsa5ca6";
        public static Options Options { get; private set; }

        public static CaptureModule Capture { get; private set; } = null;
        public static ProcessModule Process { get; private set; } = null;
        public static DeliverModule Deliver { get; private set; } = null;
        public static PersistModule Persist { get; private set; } = null;

        public static Network Network { get; private set; } = null;
        public static DatabaseContext Context { get; private set; }
        public static List<AbstractLibrary> Libraries { get; set; } = new List<AbstractLibrary>();

        public static List<RoomDefinition> LocalRooms { get; set; } = null;

        public static Dictionary<string, Command> Commands=new Dictionary<string, Command>();

        public static T GetLibrary<T>() where T : AbstractLibrary
        {
            return (T)Libraries.FirstOrDefault(lib => lib.GetType().IsEquivalentTo(typeof(T)));
        }
        
        private static void RegisterCommands(params Command[] cmds)
        {
            foreach (Command cmd in cmds)
                foreach (string alias in cmd.Aliases)
                    Commands.Add(alias, cmd);
        }

        static void Main(string[] args)
        {
            TestMessage msg = new TestMessage();
            var a=msg.MessageType;
            RegisterCommands(new CmdTestNetwork(),new CmdNetworkDiscovery(),new CmdClear(),new CmdLibraries(),new CmdModules());
            ConsoleP.WriteLine("Kazeta node (version " + version + ")");
            ConsoleP.WriteLine("=====================================");
            dynamic yamlObject;
            try
            {
                ConsoleP.Write("Reading configuration file: ");
                var deserializer = new DeserializerBuilder().Build();
                var input = new StringReader(File.ReadAllText(@"kazeta.yaml"));
                yamlObject = deserializer.Deserialize(input);
                NodeId = yamlObject["id"];
                ConsoleP.WriteLine("OK");
                ConsoleP.WriteLine("Connecting to network: ");
                Network = new Network(yamlObject);
                Network.Connect();
                ConsoleP.WriteLine("Modules:");
                ConsoleP.Write("  - Capture:  ");
                ConsoleP.WriteLine(EnableCapture() ? new string[] { "@green","enabled" } : new string[] { "@red","disabled" });
                ConsoleP.Write("  - Process:  ");
                ConsoleP.WriteLine(EnablePersist() ? new string[] { "@green", "enabled" } : new string[] { "@red", "disabled" });
                ConsoleP.Write("  - Persist:  ");
                ConsoleP.WriteLine(EnableProcess() ? new string[] { "@green", "enabled" } : new string[] { "@red", "disabled" });
                ConsoleP.Write("  - Deliver:  ");
                ConsoleP.WriteLine(EnableDeliver() ? new string[] { "@green", "enabled" } : new string[] { "@red", "disabled" });
            }
            catch(Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                ConsoleP.WriteLine("failed");
                Console.ResetColor();
                ConsoleP.WriteLine(e.ToString());
                return;
            }
            Libraries = AbstractLibrary.Initialize(yamlObject["libraries"]);
            foreach (AbstractLibrary lib in Libraries)
            {
                RegisterCommands(lib.Commands.ToArray());
                foreach (var r in lib.Messages)
                    Network.RegisterHandler(r.Item1, r.Item2);
            }
            if (true)
                InteractiveMode();
            else
                while (true) { Thread.Sleep(1_000); }
        }

        private static void InteractiveMode()
        {
            ConsoleP.WriteLine("Starting interactive mode");
            ConsoleP.ShowInterface();
            while (true)
            {
                ConsoleP.SetAutoComplete(Commands.Keys);
                string line = ConsoleP.ReadLine();
                if (line.Length == 0)
                    continue;
                var input = CommandInput.ParseLine(line);
                if (Commands.ContainsKey(input.Id))
                {
                    Commands[input.Id].Execute(null, input);
                }
                /*
                switch(split[0])
                {
                    case "version":
                        ConsoleP.WriteLine("Kazeta node " + version);
                        break;
                    case "modules":
                        ConsoleP.WriteLine("Modules:");
                        ConsoleP.Write("  - Capture:  ");
                        Console.ForegroundColor = Capture == null ? ConsoleColor.Red : ConsoleColor.Green;
                        Console.WriteLine(Capture!=null ? "enabled" : "disabled");
                        Console.ResetColor();
                        Console.Write("  - Process:  ");
                        Console.ForegroundColor = Capture == null ? ConsoleColor.Red : ConsoleColor.Green;
                        Console.WriteLine(Process!=null ? "enabled" : "disabled");
                        Console.ResetColor();
                        Console.Write("  - Deliver:  ");
                        Console.ForegroundColor = Capture == null ? ConsoleColor.Red : ConsoleColor.Green;
                        Console.WriteLine(Deliver != null ? "enabled" : "disabled");
                        Console.ResetColor();
                        Console.Write("  - Persist:  ");
                        Console.ForegroundColor = Capture == null ? ConsoleColor.Red : ConsoleColor.Green;
                        Console.WriteLine(Persist != null ? "enabled" : "disabled");
                        Console.ResetColor();
                        break;
                    case "libraries":
                        Console.WriteLine("Libraries:");
                        foreach(var library in Libraries)
                            Console.WriteLine("  - "+library.Name+" ( "+library.Version+" )");
                        break;
                    case "work-builder":
                        (new InteractiveWorkBuilder()).Start();
                        break;
                    case "free":
                        DriveInfo info = new DriveInfo(split[1]);
                        Console.WriteLine("Free space: "+info.AvailableFreeSpace);
                        break;
                    case "room":
                        break;
                    case "discovery":
                        Postman.SendToChannel(Channels.Discovery, new DiscoveryRequest())
                            .Then( a => { a.ForEach(b => Console.WriteLine("Found node:  " + b.Sender));return true; })
                            .Timeout(6000,()=> { Console.WriteLine("No node found"); return true; })
                            .ExpectMultiple().Send();
                        break;
                    case "ndi":
                        switch (split[1])
                        {
                            case "scan": break;
                            case "snapshot": break;
                        }
                        break;
                }*/

            }
        }

        private static bool EnableCapture()
        {
            Capture = new CaptureModule();
            return true;
        }
        private static bool EnableProcess()
        {
            Process = new ProcessModule();
            return true;
        }
        private static bool EnablePersist()
        {
            Persist = new PersistModule();
            return true;
        }
        private static bool EnableDeliver()
        {
            return false;
        }
    }
}
