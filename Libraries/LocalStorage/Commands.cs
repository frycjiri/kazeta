﻿using KazetaNode.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace KazetaNode.Libraries.LocalStorage
{
    class CmdLocalStorageStatus : Command
    {
        private LocalStorage localStorage;

        public CmdLocalStorageStatus(LocalStorage localStorage)
        {
            this.localStorage = localStorage;
        }

        public override string[] Aliases => new string[] { "storage-status" };

        public override void Execute(AbstractLibraryInstance instance, CommandInput input)
        {
            ConsoleP.AppendDelimeter();
            ConsoleP.AppendCols(new int[] { 16,16,16,16,16 },"Drive","Path","Used","Max","Free");
            ConsoleP.AppendDelimeter();
            foreach (var loc in localStorage.Locations) {
                DriveInfo info = new DriveInfo(loc.FullPath);
                ConsoleP.AppendCols(new int[] { 16, 16, 16, 16, 16 }, info.Name,loc.RelativePath, FileSystemHelper.DirSize(Directory.CreateDirectory(loc.FullPath)) / 1048576 +"MB", loc.MaxSize+"MB",info.AvailableFreeSpace/1048576+"MB");
            }
            ConsoleP.AppendDelimeter();
        }
    }
}
