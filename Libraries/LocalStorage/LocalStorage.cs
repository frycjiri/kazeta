﻿using System;
using System.Collections.Generic;
using System.Text;
using KazetaNode.Coms;
using System.IO;

namespace KazetaNode.Libraries.LocalStorage
{
    class LocalStorage : AbstractLibrary
    {
        public override string Id => "localstorage";

        public override string Name => "Local storage";

        public override string Version => "1.0";

        public override List<Command> Commands => new List<Command>() { new CmdLocalStorageStatus(this) };
        
        public override List<Tuple<Type, Network.ReceiveMessage>> Messages => new List<Tuple<Type, Network.ReceiveMessage>>();

        protected internal List<LocalStorageLocation> Locations = new List<LocalStorageLocation>();

        public LocalStorage(dynamic config)
        {
            foreach(dynamic loc in config)
            {
                Locations.Add(new LocalStorageLocation()
                {
                    FullPath = Path.GetFullPath(loc["path"]),
                    RelativePath = loc["path"],
                    MaxSize =Int32.Parse(loc["max_usage"]),
                    Primary=Boolean.Parse(loc["primary"])
                });
            }
        }
        public override AbstractLibraryInstance CreateCaptureInstance()
        {
            throw new NotImplementedException();
        }
    }
    class LocalStorageLocation
    {
        public string FullPath { get; protected internal set; }
        public string RelativePath { get; protected internal set; }
        public int MaxSize { get; protected internal set; } = int.MaxValue;
        public bool Primary { get; protected internal set; } = true;
    }
}
