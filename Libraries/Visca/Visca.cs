﻿using System;
using System.Collections.Generic;
using System.Text;
using KazetaNode.Coms;

namespace KazetaNode.Libraries.Visca
{
    class Visca : AbstractLibrary
    {
        public override string Id => "visca";

        public override string Name => "Visca";

        public override string Version => "1.0";

        public override List<Command> Commands => new List<Command>();

        public override List<Tuple<Type, Network.ReceiveMessage>> Messages => new List<Tuple<Type, Network.ReceiveMessage>>();

        public override AbstractLibraryInstance CreateCaptureInstance()
        {
            throw new NotImplementedException();
        }
    }
}
