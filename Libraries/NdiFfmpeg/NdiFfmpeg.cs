﻿using KazetaNode.Helpers;
using KazetaNode.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace KazetaNode.Libraries.NdiFfmpeg
{
    class NdiFfmpeg : Ffmpeg.Ffmpeg
    {
        public override string Name { get => "ndi_ffmpeg"; }
        public override string Version { get => "1.0"; }
        public static List<NdiSource> FindNdiSources()
        {
            Regex regex = new Regex(@"^\s*\[libndi_newtek\s*\@\s*\w*\]\s*'([^-]*)-([^\s]*)\s*\(([^\)]*)\)'\s*'([^:]*):([^']*)'\s*$");
            var result = Shell.Execute(Ffmpeg.Ffmpeg.CMD_PROGRAM, CMD_FIND_SOURCES, 5_000);
            if (result.ExitCode != 0)
                throw new Exception();
            var list = new List<NdiSource>();
            string line;
            while ((line = result.Output.ReadLine()) != null)
            {
                var match = regex.Match(line);
                if (!match.Success)
                    continue;
                list.Add(new NdiSource()
                {
                    Id = match.Groups[1].Value,
                    Ip = match.Groups[4].Value,
                });
            }
            return list;
        }

        public static void TakeScreenshot(NdiSource source, int quality = 1, int width = -1, int height = -1)
        {
            var param = PARAM_QUALITY.Param("value", quality);
            param += (width == height && width == -1) ? "" : (" " + PARAM_SCALE.Param("width", width).Param("height", height));
            var cmd = CMD_TAKE_SCREENSHOT.Param("params", param).Param("input", source.Id);
        }
    }
}
