﻿using System;
using System.Collections.Generic;
using System.Text;
using KazetaNode.Coms;
using KazetaNode.Helpers;
using System.Text.RegularExpressions;
using KazetaNode.Models;
using System.Runtime.InteropServices;
using System.IO;

namespace KazetaNode.Libraries.Ffmpeg
{
    class Ffmpeg : AbstractLibrary
    {
        protected internal const string CMD_FIND_SOURCES = "-f libndi_newtek -find_sources 1 -i dummy";
        protected internal const string CMD_TAKE_SCREENSHOT = "-i %input% -vframes 1 %params% %tempfile%";
        protected internal const string CMD_CAPTURE = "";

        protected internal const string PARAM_QUALITY = "-q:v %value%";
        protected internal const string PARAM_SCALE = "-filter:v scale=\"%width%:%height%\"";

        public static string CMD_PROGRAM { get
            {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                    return "Libraries/Ffmpeg/ffmpeg.exe";
                return "ffmpeg";
            } }

        public override string Name { get => "ffmpeg";  }
        public override string Version { get => "1.0"; }

        public override string Id => "ffmpeg";

        public override List<Command> Commands => new List<Command>() { new CmdNdiFindSources(), new CmdNdiTakeScreenshot(),new CmdNdiCapture() };

        public override List<Tuple<Type, Network.ReceiveMessage>> Messages => new List<Tuple<Type, Network.ReceiveMessage>>();

        public override AbstractLibraryInstance CreateCaptureInstance()
        {
            throw new NotImplementedException();
        }
    }
    class CmdNdiFindSources : Command
    {
        public override string[] Aliases => new string[] { "ndi-scan" };

        public override void Execute(AbstractLibraryInstance instance, CommandInput input)
        {
            Regex regex = new Regex(@"^\s*\[libndi_newtek\s*\@\s*\w*\]\s*'([^-]*)-([^\s]*)\s*\(([^\)]*)\)'\s*'([^:]*):([^']*)'\s*$");
            var result = Shell.Execute(Ffmpeg.CMD_PROGRAM,Ffmpeg.CMD_FIND_SOURCES, 5_000);
            if (result.ExitCode >1)
                throw new Exception();
            var list = new List<NdiSource>();
            string line;
            StreamReader reader=result.ExitCode==0?result.Output:result.Error;
            while ((line = reader.ReadLine()) != null)
            {
                var match = regex.Match(line);
                if (!match.Success)
                    continue;
                list.Add(new NdiSource()
                {
                    Id = match.Groups[1].Value,
                    Ip = match.Groups[4].Value,
                });
            }
            if(list.Count==0)
                ConsoleP.AppendLine("Found 0 NDI sources");
            else
                ConsoleP.AppendLine("Found " + list.Count + " NDI sources:");
            foreach(NdiSource source in list)
                ConsoleP.AppendLine("  - "+source.Id+" ("+source.Ip+")");
        }
    }
    class CmdNdiTakeScreenshot : Command
    {
        public override string[] Aliases => new string[] { "ndi-screenshot" };

        public override void Execute(AbstractLibraryInstance instance, CommandInput input)
        {
            throw new NotImplementedException();
        }
    }
    class CmdNdiCapture : Command
    {
        public override string[] Aliases => new string[] { "ndi-capture" };

        public override void Execute(AbstractLibraryInstance instance, CommandInput input)
        {
            throw new NotImplementedException();
        }
    }
}
