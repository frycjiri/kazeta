﻿using KazetaNode.Coms;
using System;
using System.Collections.Generic;
using System.Text;
using static KazetaNode.Coms.Network;

namespace KazetaNode.Libraries
{
    abstract class AbstractLibraryInstance
    {

    }
    abstract class AbstractLibrary
    {
        public abstract string Id { get; }
        public abstract string Name { get; }
        public abstract string Version { get;}
        public abstract AbstractLibraryInstance CreateCaptureInstance();

        public abstract List<Command> Commands { get; }
        public abstract List<Tuple<Type, ReceiveMessage>> Messages { get; }

        private static List<AbstractLibrary> libraries =null;
        public static List<AbstractLibrary> Initialize(dynamic libraryConfig)
        {
            if (libraries == null)
                libraries = new List<AbstractLibrary>
            {
                new Ffmpeg.Ffmpeg(),
                new KosApi.KosApi(libraryConfig?["kosapi"]),
                new Room.Room(),
                new LocalStorage.LocalStorage(libraryConfig?["localstorage"]),
                new Visca.Visca(),
                new TaskScheduler.TaskScheduler(),
                new FileShare.FileShare(),
                new ArtNet.ArtNet()
            };
            return libraries;
        }
    }
}
