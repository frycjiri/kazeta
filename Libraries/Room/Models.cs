﻿using KazetaNode.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace KazetaNode.Libraries.Room
{
    class RoomsModel
    {
        public List<RoomModel> rooms { get; set; } = new List<RoomModel>();
    }
    class RoomTrigger
    {
        public List<CaptureMode> Only { get; set; } = new List<CaptureMode>();
        public List<string> Script { get; set; } = new List<string>();
    }

    class RoomModelBasic
    {
        public string Id { get; set; }
        public List<CaptureMode> Modes { get; set; } = new List<CaptureMode>();
        public List<VideoQuality> Capability { get; set; } = new List<VideoQuality>();

    }
        class RoomModel : RoomModelBasic
    {
        public List<RoomTrigger> Capture { get; set; }
        public List<RoomTrigger> Terminate { get; set; }
        public List<RoomTrigger> Initialize { get; set; }
        public List<NdiSourceDescriptor> Sources { get; set; } = new List<NdiSourceDescriptor>();
    }
}
