﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace KazetaNode.Libraries.Room
{
    class CmdRoomInfo : Command<Room>
    {
        public CmdRoomInfo(Room library) : base(library)
        {
        }

        public override string[] Aliases => new string[] { "room-local-detail" };

        public override void Execute(AbstractLibraryInstance instance, CommandInput input)
        {
            var room = library.LocalRooms[input.Arguments[0]];
            ConsoleP.AppendDelimeter();
            ConsoleP.AppendLine("Capability: ", "@darkgreen", String.Join(", ", room.Capability.Select(c => c.ToString())));
            ConsoleP.AppendLine("Modes: ", "@darkgreen", String.Join(", ", room.Modes.Select(c => c.ToString())));
            ConsoleP.AppendDelimeter();
            if (input.Parameters.ContainsKey("a") || input.Parameters.ContainsKey("scripts"))
            {
                ConsoleP.AppendLine("Inicializate script:");
                foreach (var terminate in room.Initialize)
                {
                    ConsoleP.AppendLine("[ONLY= ", "@darkblue", String.Join(", ", terminate.Only.Select(c => c.ToString())), "@reset", " ]");
                    foreach (var line in terminate.Script)
                        ConsoleP.AppendLine(" - " + line);
                }
                ConsoleP.AppendDelimeter();
                ConsoleP.AppendLine("Record script:");
                foreach (var terminate in room.Capture)
                {
                    ConsoleP.AppendLine("[ONLY= ", "@darkblue", String.Join(", ", terminate.Only.Select(c => c.ToString())), "@reset", " ]");
                    foreach (var line in terminate.Script)
                        ConsoleP.AppendLine(" - " + line);
                }
                ConsoleP.AppendDelimeter();
                ConsoleP.AppendLine("Terminate script:");
                foreach (var terminate in room.Terminate)
                {
                    ConsoleP.AppendLine("[ONLY= ", "@darkblue", String.Join(", ", terminate.Only.Select(c => c.ToString())), "@reset", " ]");
                    foreach (var line in terminate.Script)
                        ConsoleP.AppendLine(" - " + line);
                }
                ConsoleP.AppendDelimeter();
            }
            if (input.Parameters.ContainsKey("a") || input.Parameters.ContainsKey("sources"))
            {
                ConsoleP.AppendLine("Sources:");
                ConsoleP.AppendCols(new int[] { 20, 20, 20, 5, 5, 7 }, "IP", "ID", "Name", "Audio", "Video", "Priority");
                foreach (var terminate in room.Sources)
                {
                    ConsoleP.AppendCols(new int[] { 20, 20, 20, 5, 5, 7 }, "@green", terminate.Ip + ":" + terminate.Port, terminate.Id, terminate.DisplayName, terminate.AudioCapture ? "Yes" : "No", terminate.VideoCapture ? "Yes" : "No", terminate.Priority + "");
                }
                ConsoleP.AppendDelimeter();
            }
        }
    }
    class CmdLocalRoomInfo : Command<Room>
    {
        public CmdLocalRoomInfo(Room library) : base(library)
        {
        }

        public override string[] Aliases => new string[] { "room-local" };

        public override void Execute(AbstractLibraryInstance instance, CommandInput input)
        {
            ConsoleP.AppendDelimeter();
            ConsoleP.AppendCols(new int[] { 10,10 }, "Room","Capture sources");
            ConsoleP.AppendDelimeter();
            foreach (var room in library.LocalRooms)
            {
                ConsoleP.AppendCols(new int[] { 10,10 }, room.Key,room.Value.Sources.Count+"");
            }
            ConsoleP.AppendDelimeter();
        }
    }
    class CmdRemoteRoomInfo : Command<Room>
    {
        public CmdRemoteRoomInfo(Room library) : base(library)
        {
        }

        public override string[] Aliases => new string[] { "room-remote" };

        public override void Execute(AbstractLibraryInstance instance, CommandInput input)
        {
        }
    }
    class CmdRoomCacheRefresh : Command<Room>
    {
        public CmdRoomCacheRefresh(Room library) : base(library)
        {
        }

        public override string[] Aliases => new string[] { "cache-refresh-room" };

        public override void Execute(AbstractLibraryInstance instance, CommandInput input)
        {
            this.library.Update();
        }
    }
}
