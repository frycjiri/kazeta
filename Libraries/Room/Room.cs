﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using KazetaNode.Coms;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace KazetaNode.Libraries.Room
{
    class Room : AbstractLibrary
    {
        public override string Id => "room";

        public override string Name => "Room";

        public override string Version => "1.0";

        public override List<Command> Commands => new List<Command>() {
            new CmdRoomInfo(this),new CmdRoomCacheRefresh(this),new CmdLocalRoomInfo(this),new CmdRemoteRoomInfo(this)
        };

        public override List<Tuple<Type, Network.ReceiveMessage>> Messages => new List<Tuple<Type, Network.ReceiveMessage>>()
        {
            Tuple.Create<Type,Network.ReceiveMessage>(typeof(MsgRoomSynchronizeRequest),ReceiveRequest)
        };

        protected internal void ReceiveRequest(IMessage msg)
        {
            msg.Response(new MsgRoomSynchronizeResponse()
            {
                Rooms= LocalRooms.Values.ToList<RoomModelBasic>()
            }).Send(false);
        }

        protected internal Dictionary<string, RoomModel> LocalRooms { get; set; } = new Dictionary<string, RoomModel>();
        protected internal Dictionary<string, Tuple<HashSet<string>, RoomModelBasic>> RemoteRooms { get; set; } = new Dictionary<string, Tuple<HashSet<string>, RoomModelBasic>>();

        public override AbstractLibraryInstance CreateCaptureInstance()
        {
            throw new NotImplementedException();
        }

        public Room()
        {
            var deserializer = new DeserializerBuilder().IgnoreUnmatchedProperties().WithNamingConvention(new CamelCaseNamingConvention()).Build();
            var input = new StringReader(File.ReadAllText(@"rooms.yaml"));
            RoomsModel yamlObject = deserializer.Deserialize<RoomsModel>(input);
            foreach (var r in yamlObject.rooms)
                LocalRooms.Add(r.Id, r);
        }

        public void Update()
        {
            Postman.SendToChannel(Channels.Discovery, new MsgRoomSynchronizeRequest()).During(
                msg_raw =>
                {
                    MsgRoomSynchronizeResponse msg = (MsgRoomSynchronizeResponse)msg_raw;
                        foreach (RoomModelBasic room in msg.Rooms)
                        {
                            if (!RemoteRooms.ContainsKey(room.Id))
                                RemoteRooms.Add(room.Id, Tuple.Create<HashSet<string>, RoomModelBasic>(new HashSet<string>() { msg.Sender }, room));
                            RemoteRooms[room.Id].Item1.Add(msg.Sender);
                            ConsoleP.AppendLine("Room '" + room.Id + "' capturable from Node '" + msg.Sender + "'.");
                        }
                }).ExpectMultiple().Timeout(5_000).Send();
        }

        public bool RoomExist(string room)
        {
            return LocalRooms.ContainsKey(room) || RemoteRooms.ContainsKey(room);
        }
        public HashSet<string> RemoteRoomNodes(string room)
        {
            return RemoteRooms.ContainsKey(room) ? RemoteRooms[room].Item1:null;
        }
        public RoomModelBasic RoomDescription(string room)
        {
            return LocalRooms.ContainsKey(room) ? LocalRooms[room] : (RemoteRooms.ContainsKey(room) ? RemoteRooms[room].Item2 : null);
        }
    }
}
