﻿using KazetaNode.Coms;
using System;
using System.Collections.Generic;
using System.Text;

namespace KazetaNode.Libraries.Room
{
    class MsgRoomSynchronizeRequest : IMessage
    {
    }
    class MsgRoomSynchronizeResponse : IMessage
    {
        public List<RoomModelBasic> Rooms { get; protected internal set; } = new List<RoomModelBasic>();
    }
}
