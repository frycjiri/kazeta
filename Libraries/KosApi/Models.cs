﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace KazetaNode.Libraries.KosApi
{
    class KosCourse
    {
        public string Code { get; set; }
        public string NameCs { get; set; }
        public string NameEn { get; set; }
        public HashSet<string> Teachers { get; set; } = new HashSet<string>();
        public List<KosCourseParallel> Parallels { get; set; } = new List<KosCourseParallel>();
    }
    enum Parity { ODD,EVEN,BOTH}
    enum ParallelType { Tutorial, Lecture, Laboratory}
    class KosCourseParallel
    {
        public int Occupied { get; set; }
        public int Capacity { get; set; }
        public DayOfWeek Day { get; set; }
        public int From { get; set; }
        public int[] FromHM { get { return new int[] { From / 60, From % 60 }; } }
        public int To { get; set; }
        public int[] ToHM { get { return new int[] { To / 60, To % 60 }; } }
        public Parity Parity { get; set; }
        public ParallelType Type { get; set; }
        public string RoomId { get; set; }
    }

    class KosRoomResponse
    {
        [JsonProperty("events")]
        public List<KosRoomEvent> Events { get; set; } = new List<KosRoomEvent>();
    }
    class KosRoomEventLinks
    {
        [JsonProperty("course")]
        public string Course { get; set; }
        [JsonProperty("room")]
        public string Room { get; set; }
        [JsonProperty("teachers")]
        public List<string> Teachers { get; set; } = new List<string>();
        [JsonProperty("students")]
        public List<string> Students { get; set; } = new List<string>();
    }
    
    class KosRoomEvent
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("sequence_number")]
        public int SequenceNumber { get; set; }
        [JsonProperty("starts_at")]
        public DateTime StartsAt { get; set; }
        [JsonProperty("ends_at")]
        public DateTime EndsAt { get; set; }
        [JsonProperty("deleted")]
        public bool Deleted { get; set; }
        [JsonProperty("capacity")]
        public int Capacity { get; set; }
        [JsonProperty("occupied")]
        public int Occupied { get; set; }
        [JsonProperty("event_type")]
        public string EventType { get; set; }
        [JsonProperty("parallel")]
        public string Parallel { get; set; }
        [JsonProperty("links")]
        public KosRoomEventLinks Links { get; set; } = new KosRoomEventLinks();
    }
}
