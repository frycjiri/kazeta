﻿using KazetaNode.Coms;
using KazetaNode.Helpers;
using KazetaNode.Libraries.TaskScheduler;
using KazetaNode.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static KazetaNode.Helpers.Extensions;

namespace KazetaNode.Libraries.KosApi
{
    class CmdKosApiRoomInfo : Command<KosApi>
    {
        public CmdKosApiRoomInfo(KosApi library) : base(library)
        {
        }

        public override string[] Aliases => new string[] { "kosapi-info-room" };

        public override void Execute(AbstractLibraryInstance instance, CommandInput input)
        {
            var res = library.Endpoint.RoomEvents(input.Arguments[0]);
            res.Wait();
            var response = res.Result;
            ConsoleP.AppendDelimeter();
            ConsoleP.AppendCols(new int[] { 20, 20, 20, 10 }, "course","from","to", "type");
            foreach(KosRoomEvent ev in response.Events)
            {
                ConsoleP.AppendCols(new int[] { 20, 20, 20, 10 }, ev.Links.Course, ev.StartsAt.ToString("dd/MM/yy HH:mm"), ev.EndsAt.ToString("dd/MM/yy HH:mm"), ev.EventType);
            }
            ConsoleP.AppendDelimeter();
        }
    }
    class CmdKosApiCourseInfo : Command<KosApi>
    {
        public CmdKosApiCourseInfo(KosApi library) : base(library)
        {
        }

        public override string[] Aliases => new string[] { "kosapi-info-course" };

        public override void Execute(AbstractLibraryInstance instance, CommandInput input)
        {
            var res = library.Endpoint.CourseInfoAsync(input.Arguments[0]); // B4M36ESW
            res.Wait();
            var course=res.Result;
            if (course == null)
            {
                ConsoleP.AppendLine("Course with code '" + input.Arguments[0] + "' not found");
                return;
            }
            ConsoleP.AppendLine("Course '" + course.Code + "'");
            ConsoleP.AppendLine("Název: '" + course.NameCs);
            ConsoleP.AppendLine("Name: " + course.NameEn);
            ConsoleP.AppendLine("Teachers: " + String.Join(", ", course.Teachers));
            ConsoleP.AppendDelimeter();
            ConsoleP.AppendCols(new int[] { 10, 10, 10, 10, 10 }, "Type","Day","Room","From","To");
            ConsoleP.AppendDelimeter();
            foreach (var par in course.Parallels)
            {
                ConsoleP.AppendCols(new int[] { 10,10,10,10,10 },par.Type.ToString(), par.Day.ToString(),Program.GetLibrary<Room.Room>().RoomExist(par.RoomId)?"@green": "@red", par.RoomId,"@reset",par.FromHM[0]+":"+par.FromHM[1],par.ToHM[0]+":"+par.ToHM[1]);
            }
            ConsoleP.AppendDelimeter();
        }
    }
    class CmdKosApiCaptureCourseEvent : Command<KosApi>
    {
        public CmdKosApiCaptureCourseEvent(KosApi library) : base(library)
        {
        }

        public override string[] Aliases => new string[] { "kosapi-course-capture"};

        public override void Execute(AbstractLibraryInstance instance, CommandInput input)
        {
            if (input.Arguments.Count != 1 && input.Arguments.Count != 3)
            {
                ConsoleP.AppendLine("@red", "Help: kosapi-course-capture {code} ");
                ConsoleP.AppendLine("@red", "Syntax: kosapi-course-capture {code} {parallelid} {numberofweeks}");
                return;
            }
            var res = library.Endpoint.CourseInfoAsync(input.Arguments[0]); // B4M36ESW
            res.Wait();
            var course = res.Result;
            if (course == null)
            {
                ConsoleP.AppendLine("Course with code '" + input.Arguments[0] + "' not found");
                return;
            }
            int id = 0;
            if (input.Arguments.Count == 1)
            {
                ConsoleP.AppendDelimeter();
                ConsoleP.AppendCols(new int[] { 5, 10, 10, 10, 10, 10 }, "ID", "Type", "Day", "Room", "From", "To");
                ConsoleP.AppendDelimeter();
                foreach (var par in course.Parallels)
                    ConsoleP.AppendCols(new int[] { 5, 10, 10, 10, 10, 10 }, id++ + "", par.Type.ToString(), par.Day.ToString(), Program.GetLibrary<Room.Room>().RoomExist(par.RoomId) ? "@green" : "@red", par.RoomId, "@reset", par.FromHM[0] + ":" + par.FromHM[1], par.ToHM[0] + ":" + par.ToHM[1]);
                ConsoleP.AppendDelimeter();
                return;
            }
            id = int.Parse(input.Arguments[1]);
            var parallel = course.Parallels[id];
            if (input.Arguments.Count == 2)
            {
                ConsoleP.AppendLine("@red", "Missing number of weeks");
                ConsoleP.AppendLine("@red", "Syntax: kosapi-course-capture {code} {parallelid} {numberofweeks}");
                return;
            }
            id = int.Parse(input.Arguments[2]);
            List<Project> projects = new List<Project>();
            while (id > 0)
            {
                Project project = new Project
                {
                    Author = Program.NodeId
                };
                project.Id = ProjectIdGenerator.Generate(parallel.RoomId, project.Capture.From.Year, project.Capture.From.Month, project.Capture.From.Day, project.Capture.From.Hour, "kosapi");
                project.Stage = ProjectStage.NOT_STARTED;
                project.Capture.From = DateTime.Now.GetNextWeekday(parallel.Day).Date + new TimeSpan(parallel.FromHM[0], parallel.FromHM[1], 0);
                project.Capture.To = DateTime.Now.GetNextWeekday(parallel.Day).Date + new TimeSpan(parallel.ToHM[0], parallel.ToHM[1], 0);
                project.Capture.Quality = project.Capture.Quality = VideoQuality.V1080p;
                project.Capture.Room = parallel.RoomId;
                project.Capture.Mode = CaptureMode.ALL;
                project.Process.MappedFiles.Add(new ProjectProcessMappedFiles()
                {
                    FileName = "output_1080.mp4",
                    Quality = VideoQuality.V1080p
                });
                project.Process.MappedFiles.Add(new ProjectProcessMappedFiles()
                {
                    FileName = "output_720.mp4",
                    Quality = VideoQuality.V720p
                });
                project.Process.MappedFiles.Add(new ProjectProcessMappedFiles()
                {
                    FileName = "output_480.mp4",
                    Quality = VideoQuality.V480p
                });
                project.Process.Script.Add("ffmpeg -i %primary_video% -i %primary_audio% -c:a aac -c:v libx264 -strict experimental merged.mp4");
                project.Process.Script.Add("ffmpeg -i merged.mp4 -filter:v scale=1080:-1 output_1080.mp4");
                project.Process.Script.Add("ffmpeg -i merged.mp4 -filter:v scale=720:-1 output_720.mp4");
                project.Process.Script.Add("ffmpeg -i merged.mp4 -filter:v scale=480:-1 output_480.mp4");
                projects.Add(project);
                id--;
            }


            Room.Room room=Program.GetLibrary<Room.Room>();
            if(room.LocalRooms.ContainsKey(parallel.RoomId))
            {
                ConsoleP.AppendLine("Local room found.");
                foreach (Project p in projects)
                    Program.GetLibrary<TaskScheduler.TaskScheduler>().Scheduler.Add(new CaptureSchedulableTask()
                    {
                        Project = p,
                        RunAt=p.Capture.From
                });
            }
            else if(room.RemoteRooms.ContainsKey(parallel.RoomId))
            {
                ConsoleP.AppendLine("Remote room found.");
                foreach (Project p in projects)
                    Postman.SendToNode(room.RemoteRooms[parallel.RoomId].Item1.First(), new MsgTaskAdd()
                    {
                        Task = new CaptureSchedulableTask()
                        {
                            Project = p,
                            RunAt = p.Capture.From
                        }
                    });
            }
            else
            {
                ConsoleP.AppendLine("Room not found");
                return;
            }
        }
    }
}
