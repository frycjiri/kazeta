﻿using System;
using System.Collections.Generic;
using System.Text;
using KazetaNode.Coms;

namespace KazetaNode.Libraries.KosApi
{
    class KosApi : AbstractLibrary
    {
        public override string Id => "kosapi";

        public override string Name => "KosApi";

        public override string Version => "1.0";

        public override List<Command> Commands => new List<Command>() { new CmdKosApiCourseInfo(this),new CmdKosApiRoomInfo(this),new CmdKosApiCaptureCourseEvent(this)};

        public override List<Tuple<Type, Network.ReceiveMessage>> Messages => new List<Tuple<Type, Network.ReceiveMessage>>();

        protected internal RestEndpoint Endpoint { get; private set; }

        public KosApi(dynamic config)
        {
            Endpoint = new RestEndpoint(config["clientid"], config["clientsecret"]);
        }

        public override AbstractLibraryInstance CreateCaptureInstance()
        {
            throw new NotImplementedException();
        }
    }
}
