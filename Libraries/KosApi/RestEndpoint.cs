﻿using KazetaNode.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Xml.Linq;

namespace KazetaNode.Libraries.KosApi
{
    class RestEndpoint
    {
        HttpClient Client,SiriusClient;
        string ClientId, ClientSecret;
        JObject Token;
        long NextTokenGeneration = 0;

        public RestEndpoint(string id,string secret)
        {
            ClientId = id;
            ClientSecret = secret;
            Client = new HttpClient
            {
                BaseAddress = new Uri("https://kosapi.feld.cvut.cz/api/3/")
            };
            SiriusClient = new HttpClient
            {
                BaseAddress= new Uri("https://sirius.fit.cvut.cz/api/v1/")
            };
            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/xml"));
            SiriusClient.DefaultRequestHeaders.Accept.Clear();
            SiriusClient.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            GetTokenAsync().Wait();

        }

        private async System.Threading.Tasks.Task<JObject> GetTokenAsync()
        {
            if (NextTokenGeneration < DateTimeOffset.Now.ToUnixTimeSeconds())
            {
                var TokenClient = new HttpClient
                {
                    BaseAddress = new Uri("https://auth.fit.cvut.cz/")
                };
                TokenClient.DefaultRequestHeaders.Accept.Clear();
                TokenClient.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                TokenClient.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(ClientId + ":" + ClientSecret)));
                var nvc = new List<KeyValuePair<string, string>>();
                nvc.Add(new KeyValuePair<string, string>("grant_type", "client_credentials"));
                var req = new HttpRequestMessage(HttpMethod.Post, "oauth/token") { Content = new FormUrlEncodedContent(nvc) };
                var res = await TokenClient.SendAsync(req);
                Token = Newtonsoft.Json.JsonConvert.DeserializeObject<JObject>(await res.Content.ReadAsStringAsync());
                NextTokenGeneration = DateTimeOffset.Now.ToUnixTimeSeconds() + Int32.Parse(Token["expires_in"].ToString()) - 10;
            }
            return Token;
        }

        public async System.Threading.Tasks.Task<KosRoomResponse> RoomEvents(string code,DateTime? from=null,DateTime? to=null)
        {
            int diff = (7 + (DateTime.Now.DayOfWeek - DayOfWeek.Monday)) % 7;
            if (from == null)
            {
                from = DateTime.Now.AddDays(-1 * diff).Date;
                from = from?.AddHours(-(int)from?.Hour);
            }
            if (to == null)
            {
                to = DateTime.Now.AddDays(-1 * diff + 8).Date;
                to = to?.AddHours(-(int)to?.Hour);
            }
            SiriusClient.DefaultRequestHeaders.Remove("Authorization");
            var t = await GetTokenAsync();
            SiriusClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + t["access_token"].ToString());
            var res = await SiriusClient.GetAsync("rooms/" + code+ "/events?limit=100&from="+from?.ToString("o") + "&to="+to?.ToString("o"));
            return Newtonsoft.Json.JsonConvert.DeserializeObject<KosRoomResponse>(await res.Content.ReadAsStringAsync());
        }
        public async System.Threading.Tasks.Task<bool> RoomExistAsync(string code)
        {
            XNamespace atom = "http://www.w3.org/2005/Atom";
            Client.DefaultRequestHeaders.Remove("Authorization");
            var t = await GetTokenAsync();
            Client.DefaultRequestHeaders.Add("Authorization", "Bearer " + t["access_token"].ToString());
            var res=await Client.GetAsync("rooms/" + code);
            return await Client.GetAsync("rooms/" + code).ContinueWith<bool>(e => e.Result.IsSuccessStatusCode);
        }
        public async System.Threading.Tasks.Task<KosCourse> CourseInfoAsync(string code,string sem="current")
        {
            KosCourse course = new KosCourse();
            try { 
            XNamespace atom = "http://www.w3.org/2005/Atom";
            XNamespace n= "http://kosapi.feld.cvut.cz/schema/3";
            XNamespace xlink = "http://www.w3.org/1999/xlink";
            XNamespace xml = XNamespace.Xml;
            Client.DefaultRequestHeaders.Remove("Authorization");
            var t = await GetTokenAsync();
            Client.DefaultRequestHeaders.Add("Authorization", "Bearer " + t["access_token"].ToString());
            var query = HttpUtility.ParseQueryString(string.Empty);
            query["limit"] = "1000";
            query["detail"] = "1";
            query["sem"] = sem;

            var res = await Client.GetAsync("courses/" + code + "?" + query.ToString());
            if (!res.IsSuccessStatusCode)
                return null;
            var body = await res.Content.ReadAsStringAsync();
            var doc = XDocument.Parse(body);
            course.Code = code;
            var names = doc.Root.Element(atom + "content")?.Elements(n + "name");
            if (names != null)
                foreach (var name in names)
                {
                    if (name.Attribute(xml + "lang").Value.Equals("cs"))
                        course.NameCs = name.Value;
                    else if (name.Attribute(xml + "lang").Value.Equals("en"))
                        course.NameEn = name.Value;
                }
            foreach (var teacher in doc.Root.Element(atom + "content")?.Element(n + "instance")?.Element(n + "lecturers")?.Elements(n + "teacher"))
            {
                var val = teacher?.Attribute(xlink + "href")?.Value.Split("/")?[1];
                if (val != null)
                    course.Teachers.Add(val);
            }
            foreach (var teacher in doc.Root.Element(atom + "content")?.Element(n + "instance")?.Element(n + "instructors")?.Elements(n + "teacher"))
            {
                var val = teacher?.Attribute(xlink + "href")?.Value.Split("/")?[1];
                if (val != null)
                    course.Teachers.Add(val);
            }
             res = await Client.GetAsync("courses/" + code+ "/parallels?"+query.ToString());
            if (!res.IsSuccessStatusCode)
                return null;
            body = await res.Content.ReadAsStringAsync();
             doc = XDocument.Parse(body);
            var elements = doc.Root.Elements(atom + "entry");
            foreach(var element in elements)
            {
                var content = element.Element(atom + "content");
                foreach (var el in content.Elements(n+"timetableSlot"))
                {
                    KosCourseParallel p = new KosCourseParallel();
                    p.Capacity = content.Element(n + "capacity").Value.ToInt32();
                    p.Occupied = content.Element(n + "occupied").Value.ToInt32();
                    p.RoomId = el.Element(n + "room").Value;
                    p.Day = (DayOfWeek)(el.Element(n + "day").Value.ToInt32());
                    switch(el.Element(n + "parity").Value)
                    {
                        default:
                        case "BOTH": p.Parity = Parity.BOTH; break;
                        case "ODD": p.Parity = Parity.ODD; break;
                        case "EVEN": p.Parity = Parity.EVEN; break;
                    }
                    int start = el.Element(n + "firstHour").Value.ToInt32()-1;
                    var duration = el.Element(n + "duration").Value.ToInt32();
                    int b = start % 2;
                    start -= start % 2;
                    p.From = (7 * 60 + 30) + (start*105/2) + b*45;
                    p.To = p.From + duration * 45;
                    switch (content.Element(n + "parallelType").Value)
                    {
                        case "LABORATORY": p.Type = ParallelType.Laboratory; break;
                        default:
                        case "LECTURE": p.Type = ParallelType.Lecture; break;
                        case "TUTORIAL": p.Type = ParallelType.Tutorial; break;
                    }
                    course.Parallels.Add(p);
                }
            }
            }
            catch (Exception e)
            {
                return null;
            }
            return course;
        }
    }
}
