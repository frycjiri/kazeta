﻿using KazetaNode.Coms;
using System;
using System.Collections.Generic;
using System.Text;
using static KazetaNode.Coms.Network;

namespace KazetaNode.Libraries.ArtNet
{
    class ArtNet : AbstractLibrary
    {
        public override string Id => "artnet";
        public override string Name => "ArtNet";

        public override string Version => "1.0";

        public override List<Command> Commands => new List<Command>() { new CmdArtNetSelect(),new CmdArtNetSend() };

        public override List<Tuple<Type, Network.ReceiveMessage>> Messages => new List<Tuple<Type, Network.ReceiveMessage>>();

        public override AbstractLibraryInstance CreateCaptureInstance()
        {
            return new ArtNetInstance(this);
        }
    }
    class ArtNetInstance : AbstractLibraryInstance
    {
        public ArtNet Parent { get; private set; }
        public ArtNetInstance(ArtNet parent)
        {
            Parent = parent;
        }
    }
}
