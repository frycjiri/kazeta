﻿using KazetaNode.Coms;
using System;
using System.Collections.Generic;
using System.Text;

namespace KazetaNode.Libraries.TaskScheduler
{
    class MsgTaskAdd : IMessage
    {
        public AbstractSchedulableTask Task { get; set; }
    }
}
