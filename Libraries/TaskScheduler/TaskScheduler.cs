﻿using System;
using System.Collections.Generic;
using System.Text;
using KazetaNode.Coms;

namespace KazetaNode.Libraries.TaskScheduler
{
    class TaskScheduler : AbstractLibrary
    {
        public override string Id => "taskscheduler";

        public override string Name => "Task scheduler";

        public override string Version => "1.0";

        public override List<Command> Commands => new List<Command>() { new CmdTaskSchedulerStatus(this),new CmdTaskSchedulerlist(this)};

        public Scheduler Scheduler = new Scheduler();

        public TaskScheduler()
        {
            Scheduler.Start();
        }
        public override List<Tuple<Type, Network.ReceiveMessage>> Messages => new List<Tuple<Type, Network.ReceiveMessage>>()
        {
            Tuple.Create<Type,Network.ReceiveMessage>(typeof(MsgTaskAdd),ReceiveTaskAdd)
        };

        protected internal void ReceiveTaskAdd(IMessage msg)
        {
            var m = (MsgTaskAdd)msg;
            Scheduler.Add(m.Task);
        }

        public override AbstractLibraryInstance CreateCaptureInstance()
        {
            throw new NotImplementedException();
        }
    }
}
