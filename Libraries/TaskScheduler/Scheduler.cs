﻿using KazetaNode.Coms;
using KazetaNode.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace KazetaNode.Libraries.TaskScheduler
{
    class Scheduler
    {
        public List<AbstractSchedulableTask> Tasks { get; private set; } = new List<AbstractSchedulableTask>();
        private Thread thread;
        public Scheduler()
        {

            if (File.Exists(@"scheduler.json"))
            {
                var j = new StringReader(File.ReadAllText(@"scheduler.json"));
                while (j.Peek() < 0)
                {
                    var json = j.ReadLine();
                    JObject dyn = JsonConvert.DeserializeObject<JObject>(json);
                    var st = dyn.GetValue("message_type").ToString();
                    Type type = typeof(AbstractSchedulableTask).Assembly.GetType(st);
                    AbstractSchedulableTask msg = (AbstractSchedulableTask)JsonConvert.DeserializeObject(json, type);
                    Tasks.Add(msg);
                }
            }


        }
        private void Save()
        {
            List<string> data = new List<string>();
            foreach (AbstractSchedulableTask t in Tasks) {
                data.Add(JsonConvert.SerializeObject(t));
                    }
            File.WriteAllLines(@"scheduler.json", data);
        }
        public void Start()
        {
            thread = new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                if (Tasks.Count == 0 || Tasks[0].RunAt.CompareTo(DateTime.Now) < 0)
                    Thread.Sleep(new TimeSpan(0, 1, 0));
                else
                {
                    Tasks[0].Execute();
                    Tasks.Remove(Tasks[0]);
                    Tasks = Tasks.OrderBy(c => c.RunAt).ToList(); //HACK
                    Save();
                }

            });
            thread.Start();
        }
        public void Stop()
        {
            thread.Abort();
        }
        public void Add(AbstractSchedulableTask task)
        {
            Tasks.Add(task);
            Tasks = Tasks.OrderBy(c => c.RunAt).ToList();
            Save();
        }

    }

    abstract class AbstractSchedulableTask
    {
        [JsonProperty("run_at")]
        public DateTime RunAt { get; set; } = DateTime.Now;
        [JsonProperty("repeat")]
        public TimeSpan? Repeat { get; set; } = null;
        [JsonProperty("message_type")]
        public string TaskType { get { return this.GetType().FullName; } }
        public abstract void Execute();
    }
    class CaptureSchedulableTask : AbstractSchedulableTask
    {
        [JsonProperty("project")]
        public Project Project { get; set; }
        public override void Execute()
        {
        }
    }
}
