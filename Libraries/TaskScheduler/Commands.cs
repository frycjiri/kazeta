﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KazetaNode.Libraries.TaskScheduler
{
    class CmdTaskSchedulerStatus : Command<TaskScheduler>
    {
        public CmdTaskSchedulerStatus(TaskScheduler library) : base(library)
        {
        }

        public override string[] Aliases => new string[] { "task-status" };

        public override void Execute(AbstractLibraryInstance instance, CommandInput input)
        {
            throw new NotImplementedException();
        }
    }
    class CmdTaskSchedulerlist : Command<TaskScheduler>
    {
        public CmdTaskSchedulerlist(TaskScheduler library) : base(library)
        {
        }

        public override string[] Aliases => new string[] { "task-list" };

        public override void Execute(AbstractLibraryInstance instance, CommandInput input)
        {
            var s = Program.GetLibrary<TaskScheduler>().Scheduler;
            ConsoleP.AppendLine("Tasks:");
            ConsoleP.AppendCols(new int[] { 30,20,20 }, "Type","Run at","Repeat");
            foreach (AbstractSchedulableTask t in s.Tasks)
            {
                ConsoleP.AppendCols(new int[] { 30,20,20 }, t.TaskType.Substring(t.TaskType.LastIndexOf(".")+1),t.RunAt.ToString(),t.Repeat==null?"No":t.Repeat.ToString());
            }
        }
    }
}
