﻿using KazetaNode.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace KazetaNode.Modules.Capture
{
    class CaptureModule : AbstractModule
    {

        protected void RunCaptureJob(CaptureJob job)
        {
            RoomDefinition room = Program.LocalRooms.Find(m => m.Id == job.Room.Id);
            if (room == null)
                throw new Exception("Room does not exist in local context.");
            List<object> libraryInstances = new List<object>();
            Program.Libraries.ForEach(lib => libraryInstances.Add(lib.CreateCaptureInstance()));
        }
    }
}
