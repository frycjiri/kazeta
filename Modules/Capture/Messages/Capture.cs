﻿using KazetaNode.Coms;
using KazetaNode.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace KazetaNode.Modules.Capture.Messages
{
    [TopicMessage]
    class CaptureAuction : IMessage
    {

    }
    [TopicMessage]
    class CaptureBid : IMessage
    {
    }
    [DirectMessage]
    class CaptureAssign : IMessage
    {

    }
}
