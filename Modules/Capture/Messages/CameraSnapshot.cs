﻿using KazetaNode.Coms;
using KazetaNode.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace KazetaNode.Modules.Capture.Messages
{
    class CameraSnapshotRequest : IMessage
    {
        public NdiSource Source { get; set; }
        public int Width { get; set; } = -1;
        public int Height { get; set; } = -1;

        private int quality = 1;
        public int Quality { get { return quality; } set { this.quality = value < 1 ? 1 : (value > 31 ? 31 : value);  } }

    }
    class CameraSnapshotResponse : IMessage
    {

        public byte[] Image { get; set; }

    }
}
