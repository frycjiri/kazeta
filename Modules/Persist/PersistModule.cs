﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace KazetaNode.Modules.Persist
{
    class PersistModule : AbstractModule
    {
        public string[] Locations { get; private set; }
        public string Url { get; private set; }
        public ushort Port { get; private set; }
        public PersistModule()
        {
        }
    }
}
