﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KazetaNode.Modules.Persist.Entities
{
    class SftpUser
    {
        public string Location { get; set; }
        public string ProjectId { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
    }
}
