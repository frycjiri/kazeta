﻿using KazetaNode.Coms;
using KazetaNode.Helpers;
using KazetaNode.Modules.Persist.Messages;
using System;
using System.Collections.Generic;
using System.IO;
using static KazetaNode.Helpers.Extensions;
using System.Text;

namespace KazetaNode.Modules.Persist
{
    class PersistMessageHandler
    {
        public void OnStorageRequest(StorageRequest request)
        {
            var response = new StorageResponse();
            var user = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".GenerateRandomString(24);
            var password = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".GenerateRandomString(48);

            foreach (var location in Program.Persist.Locations)
            {
                if (request.RequiredSize >= (new DriveInfo(location)).AvailableFreeSpace)
                    continue;
                FileSystemHelper.CreateProjectFolder(location, new ProjectIdDescriptor(request.ProjectId));

                response.MessageId = request.ResponseId;
                response.Username = user;
                response.Password = password;
                break;
            }
            
        }

        public void OnStorageSpaceQuery(StorageSpaceQuery query)
        {
            var response = new StorageSpaceResponse();
            var locations = Program.Persist.Locations;
            response.Locations = new StorageSpaceLocationResponse[locations.Length];
            HashSet<string> AlreadyMappedDrives = new HashSet<string>();
            for(int i=0;i<locations.Length;i++)
            {
                var loc = new StorageSpaceLocationResponse();
                DriveInfo info = new DriveInfo(locations[i]);
                loc.SpaceUsed = FileSystemHelper.DirSize(new DirectoryInfo(locations[i]));
                loc.ProjectCount = 0;
                foreach (var subdir in Directory.GetDirectories(locations[i]))
                    loc.ProjectCount += Directory.GetDirectories(subdir).LongLength;
                if (!AlreadyMappedDrives.Contains(info.Name))
                {
                    AlreadyMappedDrives.Add(info.Name);
                    response.TotalFreeSpace += info.AvailableFreeSpace;
                }
                
                response.TotalSpaceUsed += loc.SpaceUsed;
                response.TotalProjectCount += loc.ProjectCount;
                loc.FreeSpace = info.AvailableFreeSpace;
                response.Locations[i] = loc;
            }
        }
    }
}
