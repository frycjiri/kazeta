﻿using KazetaNode.Coms;
using System;
using System.Collections.Generic;
using System.Text;

namespace KazetaNode.Modules.Persist.Messages
{
    class StorageSpaceQuery : IMessage
    {

    }
    class StorageSpaceLocationResponse
    {
        public long SpaceUsed { get; set; }
        public long FreeSpace { get; set; }
        public long ProjectCount { get; set; }
    }
    class StorageSpaceResponse : IMessage
    {
        public long TotalSpaceUsed { get; set; }
        public long TotalFreeSpace { get; set; }
        public long TotalProjectCount { get; set; }
        public StorageSpaceLocationResponse[] Locations { get; set; }
    }
    class StorageRequest : IMessage
    {
        public string ProjectId { get; set; }
        public long RequiredSize { get; set; }
    }
    class StorageResponse  : IMessage
    {
        public string Url { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
