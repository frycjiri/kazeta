﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace KazetaNode
{
    struct Options
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string TempFolder { get; set; }

        internal void Init()
        {
        }
        public ModulesOption Modules { get; set; }
    }
    struct ModulesOption
    {
        public PersistOption Persist { get; set; }
    }
    struct PersistOption
    {
        public bool Enabled { get; set; }
        public string[] Locations { get; set; }
    }
}
