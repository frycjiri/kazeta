﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace KazetaNode
{
    class ConsoleP
    {
        private static string LinePrefix="";
        private static string Line = "";
        private static int LinePos = 0;
        private static int HistoryPos = 0;
        private static IEnumerable<string> Autocomplete;
        private static List<string> History= new List<string>();
        public static void SetAutoComplete(IEnumerable<string> autocomplete)
        {
            ConsoleP.Autocomplete = autocomplete;
        }
        public static void ShowInterface()
        {
            ClearLine();
            LinePrefix = "kazeta@" + Program.NodeId + ": ";
            Console.Write(LinePrefix);
        }
        public static string ReadLine()
        {
            Line = "";
            LinePos = 0;
            while (true)
            {
                var key = Console.ReadKey();
                switch(key.Key)
                {
                    case ConsoleKey.UpArrow:
                        if (HistoryPos > 0)
                        {
                            HistoryPos--;
                            Line = History[HistoryPos];
                            LinePos = Line.Length;
                            ClearLine();
                            Console.Write(LinePrefix + Line);
                        }
                        break;
                    case ConsoleKey.DownArrow:
                        if (HistoryPos < History.Count)
                        {
                            HistoryPos++;
                            if(HistoryPos==History.Count)
                            {
                                Line = "";
                                LinePos = Line.Length;
                                ClearLine();
                                Console.Write(LinePrefix + Line);
                            }
                            else
                            {
                                Line = History[HistoryPos];
                                LinePos = Line.Length;
                                ClearLine();
                                Console.Write(LinePrefix + Line);
                            }
                        }
                        else
                        {
                            ClearLine();
                            Console.Write(LinePrefix + Line);
                        }
                        break;
                    case ConsoleKey.LeftArrow:
                        if (LinePos > 0)
                        {
                            Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
                            LinePos--;
                        }
                        break;
                    case ConsoleKey.RightArrow:
                        if (LinePos < Line.Length)
                        {
                            Console.SetCursorPosition(Console.CursorLeft + 1, Console.CursorTop);
                            LinePos++;
                        }
                        break;
                    case ConsoleKey.Enter:
                        Console.WriteLine(LinePrefix + Line);
                        string Data = Line;
                        LinePrefix = "kazeta@" + Program.NodeId + ": ";
                        Console.Write(LinePrefix);
                        History.Add(Data);
                        HistoryPos = History.Count;
                        Line = "";
                        LinePos = 0;
                        return Data;
                    case ConsoleKey.Tab:
                        if (Line.Length == 0)
                        {
                            ClearLine();
                            Console.WriteLine(String.Join(", ", Autocomplete));
                            Console.Write(LinePrefix + Line);
                        }
                        else
                        {
                            var res = Autocomplete.Where(e => e.StartsWith(Line));
                            if (res.Count() == 1)
                            {
                                Line += res.First().Substring(Line.Length);
                                LinePos = Line.Length;
                                ClearLine();
                                Console.Write(LinePrefix + Line);
                            }
                            else
                            {
                                string ToBeAdded = "";
                                int i = Line.Length;
                                while (res.Count()>0)
                                {
                                    bool kill = false;
                                    char ch = ' ';
                                    foreach (var r in res)
                                    {
                                        if(r.Length<=i)
                                        {
                                            kill=true; break;
                                        }
                                        if (ch == ' ')
                                            ch = r[i];
                                        else if (ch != r[i])
                                        {
                                            kill = true;
                                            break;
                                        }
                                    }
                                    if (kill) break;
                                    i++;
                                    ToBeAdded += ch;
                                }
                                if(ToBeAdded.Length>0)
                                {
                                    Line += ToBeAdded;
                                    LinePos = Line.Length;
                                    ClearLine();
                                    Console.Write(LinePrefix + Line);
                                }
                                ClearLine();
                                Console.WriteLine(String.Join(", ", res));
                                Console.Write(LinePrefix + Line);
                            }
                        }
                        break;
                    case ConsoleKey.Backspace:
                        if (Line.Length > 0)
                        {
                            Line = Line.Substring(0, Line.Length - 1);
                            Console.Write(' ');
                            Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
                            LinePos--;
                        }
                        else
                            Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop);
                        break;
                    default:
                        if(LinePos<Line.Length)
                        {
                            Line = Line.Substring(0, LinePos) + key.KeyChar + Line.Substring(LinePos + 1);
                        }
                        else
                            Line += key.KeyChar;
                        LinePos++; break;
                }
                    
            }
        }

        internal static void Clear()
        {
            Console.Clear();
            LinePrefix = "kazeta@" + Program.NodeId + ": ";
            Console.Write(LinePrefix);
            History.Add(Line);
            HistoryPos = History.Count;
            Line = "";
            LinePos = 0;
        }

        public static void ClearLine(int pos=0)
        {
            Console.SetCursorPosition(0, Console.CursorTop-pos);
            Console.Write(new string(' ', Console.BufferWidth));
            Console.SetCursorPosition(0, Console.CursorTop-pos-1);
        }
        public static void AppendLine(string line)
        {
            ClearLine();
            Console.WriteLine(line);
            Console.Write(LinePrefix+Line);
        }
        public static void AppendDelimeter()
        {
            ClearLine();
            Console.Write(new string('-', Console.BufferWidth));
            Console.Write(LinePrefix + Line);
        }
        public static void AppendCols(int[] cols,params string[] items)
        {
            ClearLine();
            int i = 0;
            foreach (string item in items)
            {
                switch (item)
                {
                    case "@reset": Console.ResetColor(); break;
                    case "@red": Console.ForegroundColor = ConsoleColor.Red; break;
                    case "@black": Console.ForegroundColor = ConsoleColor.Black; break;
                    case "@green": Console.ForegroundColor = ConsoleColor.Green; break;
                    case "@white": Console.ForegroundColor = ConsoleColor.White; break;
                    case "@darkgreen": Console.ForegroundColor = ConsoleColor.DarkGreen; break;
                    case "@darkblue": Console.ForegroundColor = ConsoleColor.DarkBlue; break;
                    case "@blue": Console.ForegroundColor = ConsoleColor.Blue; break;
                    case "@yellow": Console.ForegroundColor = ConsoleColor.Yellow; break;
                    case "@gray": Console.ForegroundColor = ConsoleColor.Gray; break;
                    default: Console.Write(item.PadRight(cols[i++])); break;
                }
            }
            Console.ResetColor();
            Console.WriteLine();
            Console.Write(LinePrefix + Line);
        }
        public static void AppendLine(params string[] items)
        {
            ClearLine();
            foreach(string item in items)
            {
                switch(item)
                {
                    case "@reset": Console.ResetColor(); break;
                    case "@red": Console.ForegroundColor=ConsoleColor.Red; break;
                    case "@black": Console.ForegroundColor = ConsoleColor.Black; break;
                    case "@green": Console.ForegroundColor = ConsoleColor.Green; break;
                    case "@white": Console.ForegroundColor = ConsoleColor.White; break;
                    case "@blue": Console.ForegroundColor = ConsoleColor.Blue; break;
                    case "@darkgreen": Console.ForegroundColor = ConsoleColor.DarkGreen; break;
                    case "@darkblue": Console.ForegroundColor = ConsoleColor.DarkBlue; break;
                    case "@yellow": Console.ForegroundColor = ConsoleColor.Yellow; break;
                    case "@gray": Console.ForegroundColor = ConsoleColor.Gray; break;
                    default: Console.Write(item); break;
                }
            }
            Console.ResetColor();
            Console.WriteLine();
            Console.Write(LinePrefix + Line);
        }
        public static void WriteLine(string line)
        {
            Console.WriteLine(line);
        }
        public static void WriteLine(params string[] items)
        {
            foreach (string item in items)
            {
                switch (item)
                {
                    case "@reset": Console.ResetColor(); break;
                    case "@red": Console.ForegroundColor = ConsoleColor.Red; break;
                    case "@black": Console.ForegroundColor = ConsoleColor.Black; break;
                    case "@green": Console.ForegroundColor = ConsoleColor.Green; break;
                    case "@white": Console.ForegroundColor = ConsoleColor.White; break;
                    case "@blue": Console.ForegroundColor = ConsoleColor.Blue; break;
                    case "@yellow": Console.ForegroundColor = ConsoleColor.Yellow; break;
                    case "@gray": Console.ForegroundColor = ConsoleColor.Gray; break;
                    default: Console.Write(item); break;
                }
            }
            Console.ResetColor();
            Console.WriteLine();
        }
        public static void Write(string text)
        {
            Console.Write(text);
        }
    }
}
