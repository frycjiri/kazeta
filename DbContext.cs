﻿using KazetaNode.Modules.Persist.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace KazetaNode
{
    class DatabaseContext : DbContext
    {
        public DbSet<SftpUser> SftpUsers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=data.db");
        }
    }
}
